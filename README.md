The generated private and public keys
```
openssl genrsa -out key.pem 1024
```
```
openssl rsa -in key.pem -outform PEM -pubout -out public.pem
```