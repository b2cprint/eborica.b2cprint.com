<?php

namespace EBorica;
/**
 * This file contains PHP classes that can be used
 * to interact with the eBorica's payment gateway API
 *
 * (C) 2012 Anton Georgiev. All rights reserved.
 * This work is licensed under a Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
 */

interface EBoricaActions
{
  public function set_terminal($terminal_id);
  public function set_language($language);
  public function set_currency($currency);
  public function add_transaction($amount, $transaction_id, $description);
  public function read_response($req);
  public function get_transaction_info($request);
  public function run();
}