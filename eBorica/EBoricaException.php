<?php

namespace EBorica;

use Exception;

/**
 * eBorica Payments Exception Class
 *
 */
class EBoricaException extends Exception
{
  public function __construct($message)
  {
    echo $message;
  }
}