<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

define('PUBLIC_HTML', __DIR__);

require_once __DIR__.'/vendor/autoload.php';

use App\Main;

$app = new Main();
$app->run();