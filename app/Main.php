<?php

namespace App;

use App\Services\Request;
use EBorica\EBorica;
use EBorica\EBoricaException;

class main {
  /**
   * @throws EBoricaException
   */
  public function run()
  {
    $eBoricaResponse = Request::get('eBorica');
    if($eBoricaResponse) {
      $client = $this->getClient(EBorica::get_terminal_id_from_message($eBoricaResponse));
      $this->response($client, $eBoricaResponse);
    } else {
      $client = $this->getClient(Request::post('terminal_id'));
      if(!$client) {
        echo 'No client for this terminal id';die();
      }
      $this->request($client);
    }
  }

  /**
   * @param $client
   * @throws EBoricaException
   */
  private function request($client)
  {
    $eBorica = new EBorica($client);

    $eBorica->add_transaction(Request::post('amount'), Request::post('order_id'), Request::post('description'));
    $url = $eBorica->generate_request();
    header("Location: {$url}");
    exit();

  }

  /**
   * @param $client
   * @param $eBoricaResponse
   * @throws EBoricaException
   */
  private function response($client, $eBoricaResponse)
  {
    $eBorica = new EBorica($client);
    $response_data = $eBorica->read_response($eBoricaResponse);
    $transaction_id = $response_data['transaction_id'];
    $transaction_info = $eBorica->get_transaction_info($transaction_id);
    $url = $client['url'];
    $query = http_build_query([
      'order_id' => trim($transaction_info['transaction_id']),
      'code' => $transaction_info['response_code'],
      'status' => EBorica::$response_codes[$transaction_info['response_code']],
      'supplier' => 'eborica'
    ]);
    header("Location: {$url}?{$query}");
    exit();
  }

  private function getClient($terminal_id) {
    $clients = file_get_contents(PUBLIC_HTML."/clients.json");
    $clients = json_decode($clients, true);
    foreach ($clients as $client) {
      if ($client['terminal_id'] === $terminal_id) {
        return $client;
      }
    }
    return [];
  }
}