<?php
namespace App\Services;

/**
 * Request http method
 */
class Request {
  /**
   * @param $value
   * @return bool
   */
  public static function get($value)
  {
    return isset($_GET[$value]) ? $_GET[$value] : false;
  }

  /**
   * @param $value
   * @return bool
   */
  public static function post($value)
  {
    return isset($_POST[$value]) ? $_POST[$value] : false;
  }
}
